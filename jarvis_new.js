var title; // array that stores the column names picked up from the csv file
var selecteditems = ""; // the contents of the final batch file
var title1;



		// Javascript function for upload click
		$(function() {
			$("#upload")
					.bind(
							"click",
							function() {
								var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
								if (regex.test($("#fileUpload").val()
										.toLowerCase())) {
									if (typeof (FileReader) != "undefined") {
										var reader = new FileReader();
										reader.onload = function(e) {
											var file = e.target.files;
											console.log(file);
											
											Papa.parse(file, {
															header: true,
															dynamicTyping: true,
															preview: 2,
															complete: function (results) {
																data = results;
																console.log(data);
															}
														});

											var rows = e.target.result
													.split("\n");
											alert(e.target.result.substring(0, 100));
											title = rows[0].split(",");
											var $select = $('#title');
											var $rating = $('#rating');
											var $item = $('#item');
											var $bucketInDiv = $('#bucketsInDiv');
											var $prefInDiv = $('#preferencesInDiv');

											$select.find('option').remove();
											$rating.find('option').remove();
											$item.find('option').remove();
											$bucketInDiv.find('option')
													.remove();
											$prefInDiv.find('option').remove();

											var itemLabel = document
													.createElement("Label");
											itemLabel.innerHTML = "Bucket : ";
											$('<option>').val("Choose").text(
													"Choose").appendTo($select);
											$('<option>').val("Choose").text(
													"Choose").appendTo($rating);
											$('<option>').val("Choose").text(
													"Choose").appendTo($item);

											for (var j = 0; j < title.length; j++) {
												$('<option>').val(title[j])
														.text(title[j])
														.appendTo($select);
												$('<option>').val(title[j])
														.text(title[j])
														.appendTo($rating);
												$('<option>').val(title[j])
														.text(title[j])
														.appendTo($item);
											}

											for (var j = 0; j < title.length; j++) {
												$('<option>').val(title[j])
														.text(title[j])
														.appendTo($bucketInDiv);
												$('<option>').val(title[j])
														.text(title[j])
														.appendTo($prefInDiv);
											}
											

											$("#dvCSV").html('');
										}
										reader
												.readAsText($("#fileUpload")[0].files[0]);
									} else {
										alert("This browser does not support HTML5.");
									}
								} else {
									alert("Please upload a valid CSV file.");
								}
							});

		});

		// Javascript function that saves the configuration and runs as a batch file.

		function saveTextAsFile() {
			var user = $('#title').find(":selected").text();
			var rating = $('#rating').find(":selected").text();
			var item = $('#item').find(":selected").text();

			var bucket = $('#bucketsInDiv').find(":selected").map(function() {
				return $(this).text();
			}).get();
			var preference = $('#preferencesInDiv').find(":selected").map(
					function() {
						return $(this).text();
					}).get();
			// Bucket + Preferences
			selecteditems = selecteditems + "user : " + user + " rating : "
					+ rating + " item: " + item + " bucket: " + bucket
					+ "preference : " + preference;

			var textToWrite = selecteditems;

			var textFileAsBlob = new Blob([ textToWrite ], {
				type : 'application/bat'
			});
			var fileNameToSaveAs = "jarvis.bat";

			var downloadLink = document.createElement("a");
			downloadLink.download = fileNameToSaveAs ;
			downloadLink.innerHTML = "Download File";
			if (window.webkitURL != null) {
				// Chrome allows the link to be clicked
				// without actually adding it to the DOM.
				downloadLink.href = window.webkitURL
						.createObjectURL(textFileAsBlob);
			} else {
				// Firefox requires the link to be added to the DOM
				// before it can be clicked.
				downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				downloadLink.onclick = destroyClickedElement;
				downloadLink.style.display = "none";
				document.body.appendChild(downloadLink);
			}
			downloadLink.click();
		}

		function destroyClickedElement(event) {
			document.body.removeChild(event.target);
		}

		// Javascript function to remove user, item, rating from the segmentation and preference variables
		function RemoveUserItemRatingFromBucketPreference() {
			changedTitle= title;
			var user = $('#title').find(":selected").text();
			var rating = $('#rating').find(":selected").text();
			var item = $('#item').find(":selected").text();
			
			if (((user==item) || (user==rating) || (item==rating)) && (user!="Choose") && (item!="Choose") && (rating!="Choose")) {
				alert("Alert!! User, Item, Rating should be unique");
			}
						
			changedTitle = jQuery.grep(changedTitle, function(value) {return value != user;});
			changedTitle = jQuery.grep(changedTitle, function(value) {return value != item;});	
			changedTitle = jQuery.grep(changedTitle, function(value) {return value != rating;});
				
			var $bucketInDiv = $('#bucketsInDiv');
			$bucketInDiv.find('option').remove();
			// re-populate the drop down with the remaining options
			for (var j = 0; j < changedTitle.length; j++) {
				$('<option>').val(changedTitle[j]).text(changedTitle[j])
						.appendTo($bucketInDiv);
			}
			
			var $preferencesInDiv = $('#preferencesInDiv');
			$preferencesInDiv.find('option').remove();

			for (var j = 0; j < changedTitle.length; j++) {
				$('<option>').val(changedTitle[j]).text(changedTitle[j]).appendTo(
						$preferencesInDiv);
			}
						
			// find the bucket element, remove the current options from the bucket
			var $bucketInDiv = $('#bucketsInDiv');
			$bucketInDiv.find('option').remove();
			// re-populate the drop down with the remaining options
			for (var j = 0; j < changedTitle.length; j++) {
				$('<option>').val(changedTitle[j]).text(changedTitle[j])
						.appendTo($bucketInDiv);
			}
			
			var $preferencesInDiv = $('#preferencesInDiv');
			$preferencesInDiv.find('option').remove();

			for (var j = 0; j < changedTitle.length; j++) {
				$('<option>').val(changedTitle[j]).text(changedTitle[j]).appendTo(
						$preferencesInDiv);
			}
			
		}
				

		function LoadPageMachineParameters() {
			var bucket1 = $('#bucketsInDiv').find(":selected").map(function() {
				return $(this).text();
			}).get();
			if (bucket1.length == 0) {
				document.getElementById("CassandraOrHadoop").innerHTML = "Hadoop Cluster Details";

				$("#cassandra").remove();
			} else {
				document.getElementById("CassandraOrHadoop").innerHTML = "Cassandra Cluster Details";
				$("#hadoop").remove();

			}
		}

		function togglePassword() {
			var upass = document.getElementById('upass');
			var toggleBtn = document.getElementById('toggleBtn');
			if (upass.type == "password") {
				upass.type = "text";
				toggleBtn.value = "Hide";
			} else {
				upass.type = "password";
				toggleBtn.value = "Show";
			}
		}

		
		$(document).ready(
				function() {
					var inputs = 0;
					$('#btnAdd').click(function() {
						inputs++;
						$('.btnDel:disabled').removeAttr('disabled');
						var c = $('.clonedInput:first').clone(true);						
						c.attr('id', 'addNodes' + inputs );						
						$('.clonedInput:last').after(c);
					});

					$('.btnDel').click(
							function() {
								if (confirm('Delete Node?')) {
									--inputs;
									$(this).closest('.clonedInput').remove();
									$('.btnDel').attr('disabled',
											($('.clonedInput').length < 2));
								}
							});

				});