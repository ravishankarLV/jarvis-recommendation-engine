
$(document).ready(function(){
    window.counterBuckets = 2;
    $("#btnAddBuckets").click(function () {     
    var newSegmentationDiv = $(document.createElement('div'))
         .attr("id", 'Segmentation' + counterBuckets)
		 .attr("class", 'row');

    newSegmentationDiv.after().html('<div class="col-md-12"> ' +
										'<div class="row">' +
											'<div class="col-md-6">' +
												'<input type="text" name="segmentationVariable' + counterBuckets + 
														'" id="segmentationVariable' + counterBuckets + '" value="" class = "form-control" onclick="{this.value = " ";}"/>' +
											'</div>' +
											'<div class="col-md-2">' +
												'<input type="text" name="segmentationVariable' + counterBuckets + 
														'" id="segmentationVariableIndex' + counterBuckets + '" value="" class = "form-control" onclick="{this.value = " ";}"/>' +
											'</div>' +
										'</div>' +
									'</div>' 
									);
    newSegmentationDiv.appendTo("#SegmentationDetails");
	console.log(counterBuckets);
    counterBuckets++;
     });
     $("#btnDelBucket").click(function () {
    if(counterBuckets==2){
          alert("No more segments to remove");
          return false;   }
         else {counterBuckets--;
             $("#Segmentation" + counterBuckets).remove();}
     });     });  
	 
$(document).ready(function(){
    window.counterPreferences = 2;
    $("#btnAddPreference").click(function () {     
    var newPreferenceDiv = $(document.createElement('div'))
         .attr("id", 'Preference' + counterPreferences)
		 .attr("class", 'row');

    newPreferenceDiv.after().html('<div class="col-md-12"> ' +
										'<div class="row">' +
											'<div class="col-md-6">' +
												'<input type="text" name="preferenceVariable' + counterPreferences + 
														'" id="preferenceVariable' + counterPreferences + '" value="" class = "form-control" onclick="{this.value = " ";}"/>' +
											'</div>' +
											'<div class="col-md-2">' +
												'<input type="text" name="preferenceVariable' + counterPreferences + 
														'" id="preferenceVariableIndex' + counterPreferences + '" value="" class = "form-control" onclick="{this.value = " ";}"/>' +
											'</div>' +
										'</div>' +
									'</div>' );
    newPreferenceDiv.appendTo("#PreferenceDetails");
    counterPreferences++;
     });
     $("#btnDelPreference").click(function () {
    if(counterPreferences==2){
          alert("No more buckets to remove");
          return false;   }
         else {counterPreferences--;
             $("#Preference" + counterPreferences).remove();}
     });     });  	 
	 


$(document).ready(function(){
    window.counterWorker = 2;
    $("#addButton").click(function () {     
    var newTextBoxDiv = $(document.createElement('div'))
         .attr("id", 'Worker' + counterWorker)
		 .attr("class", 'row');

    newTextBoxDiv.after().html('<div class="col-md-3"> ' +
          '<input type="text" name="workerIPAddress' + counterWorker + 
          '" id="workerIPAddress' + counterWorker + '" value="" class = "form-control"/> </div>' +
		  '<div class="col-md-3"> ' +
          '<input type="text" name="workerUsername' + counterWorker + 
          '" id="workerUsername' + counterWorker + '" value="" class = "form-control"/ > </div>'+
		  '<div class="col-md-3"> ' +
          '<input type="text" name="workerPassword' + counterWorker + 
          '" id="workerPassword' + counterWorker + '" value="" class = "form-control"/> </div>'+
		  '<div class="col-md-3"> ' +
				'<div class ="row">' +
					'<div class="col-md-12">' +
						'<label>Worker '+ counterWorker + '</label>' +						
					'</div>' +
				'</div>' +
          ' </div>' + '<br />' + '<br />');
    newTextBoxDiv.appendTo("#WorkerDetails");
    counterWorker++;
     });
     $("#removeButton").click(function () {
    if(counterWorker==2){
          alert("No more workers to remove");
          return false;   }
          counterWorker--;
             $("#Worker" + counterWorker).remove();
     });     });  


var selecteditems = ""; // the contents of the final batch file

		// Javascript function that saves the configuration and runs as a batch file.

		function CheckValuesAndDownloadBatchFile() {
			var user = $('#title').find(":selected").text();
			var rating = $('#rating').find(":selected").text();
			var item = $('#item').find(":selected").text();
			

			var bucket = $('#bucketsInDiv').find(":selected").map(function() {
				return $(this).text();
			}).get();
			var preference = $('#preferencesInDiv').find(":selected").map(
					function() {
						return $(this).text();
					}).get();
			// Bucket + Preferences
			selecteditems = selecteditems + "user : " + user + " rating : "
					+ rating + " item: " + item + " bucket: " + bucket
					+ "preference : " + preference;

			var textToWrite = selecteditems;

			var textFileAsBlob = new Blob([ textToWrite ], {
				type : 'application/bat'
			});
			var fileNameToSaveAs = "jarvis.bat";

			var downloadLink = document.createElement("a");
			downloadLink.download = fileNameToSaveAs ;
			downloadLink.innerHTML = "Download File";
			if (window.webkitURL != null) {
				// Chrome allows the link to be clicked
				// without actually adding it to the DOM.
				downloadLink.href = window.webkitURL
						.createObjectURL(textFileAsBlob);
			} else {
				// Firefox requires the link to be added to the DOM
				// before it can be clicked.
				downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				downloadLink.onclick = destroyClickedElement;
				downloadLink.style.display = "none";
				document.body.appendChild(downloadLink);
			}
			downloadLink.click();
		}

		function destroyClickedElement(event) {
			document.body.removeChild(event.target);
		}		
				

		function LoadPageMachineParameters() {		
			var x = document.getElementsByName('segmentationVariable0')[0].value;
			console.log(counterBuckets);
			if (counterBuckets == 2 && x=="Bucket1") {
				document.getElementById("CassandraOrHadoop").innerHTML = "Hadoop Cluster Details";
				$("#hadoop").show();
				$("#cassandra").hide();
			} else if (counterBuckets > 2) {
				document.getElementById("CassandraOrHadoop").innerHTML = "Cassandra Cluster Details";
				$("#hadoop").hide();
				$("#cassandra").show();

			}
		}

		function togglePassword() {
			var upass = document.getElementById('upass');
			var toggleBtn = document.getElementById('toggleBtn');
			if (upass.type == "password") {
				upass.type = "text";
				toggleBtn.value = "Hide";
			} else {
				upass.type = "password";
				toggleBtn.value = "Show";
			}
		}

		
		"ravishankarLV" 
"ravishankarLV" 
